﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveTo : MovementAction
{
    public Vector3 Goal;

    public override bool Perform(float speed)
    {
        gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, Goal, speed * Time.deltaTime);

        if(gameObject.transform.position == Goal)
        {
            return true;
        }

        return false;
    }
}
