﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class MovementAction : MonoBehaviour
{
    //Returns true when action is completed, otherwise false
    public abstract bool Perform(float speed);
}
