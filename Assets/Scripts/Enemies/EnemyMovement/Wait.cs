﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Wait : MovementAction
{
    public float Duration;

    private float currentWaitTime = 0;

    public override bool Perform(float speed)
    {
        currentWaitTime += Time.deltaTime;
        if(currentWaitTime >= Duration)
        {
            currentWaitTime = 0;
            return true;
        }
        return false;
    }
}
