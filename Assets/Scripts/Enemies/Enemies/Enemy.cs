﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    public int HP;
    public float Speed;

    public bool Invulnerable = false;

    public ObjectPooler objectPooler;

    public Shakeable shakeable;
    public List<MovementAction> movementActions;

    //private Material material;
    private Renderer[] renderers;

    private Color normalEmission;

    private float damageFlashTime = 0.1f;
    private float damageFlashIntensity = 2.5f;

    private float damageShakeDurationModifier = 0.02f;
    private float damageShakeIntensityModifier = 0.05f;
    
    private float deathDelay = 0.1f;

    public int currentMovementAction = 0;

    // Start is called before the first frame update
    public void Start()
    {
        renderers = GetComponentsInChildren<Renderer>();
        normalEmission = renderers[0].material.GetColor("_EmissionColor");
        objectPooler = ObjectPooler.Instance;
    }

    // Update is called once per frame
    public void Update()
    {
        UpdateMovement();
    }

    public void UpdateMovement()
    {
        //loops through movement actions
        //if looping is not desired, just move the enemy out of the playing field
        if(currentMovementAction < movementActions.Count)
        {
            if(movementActions[currentMovementAction].Perform(Speed))
            {
                currentMovementAction++;
                currentMovementAction = currentMovementAction % movementActions.Count;
            }
        }
    }

    public void TakeDamage(int damage)
    {
        if(!Invulnerable)
        {
            HP -= damage;
            StartCoroutine("DamageFlash");

            if (shakeable != null)
            {
                StartCoroutine(shakeable.Shake(damageShakeDurationModifier * damage, damageShakeIntensityModifier * damage));
            }

            if (HP <= 0)
            {
                StartCoroutine(Die());
            }
        }
    }

    IEnumerator DamageFlash()
    {
        float currentDelay = 0f;

        foreach (Renderer ren in renderers)
        {
            ren.material.SetColor("_EmissionColor", Color.yellow * damageFlashIntensity);
        }

        while (currentDelay < damageFlashTime)
        {
            currentDelay += Time.deltaTime;
            yield return null;
        }

        foreach (Renderer ren in renderers)
        {
            ren.material.SetColor("_EmissionColor", normalEmission);
        }
    }

    public virtual IEnumerator Die()
    {
        float currentDelay = 0f;

        while(currentDelay < deathDelay)
        {
            currentDelay += Time.deltaTime;
            yield return null;
        }
        
        gameObject.SetActive(false);
    }
}
