﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickShooter : Enemy
{
    private float ShootDelay = 0.5f;
    private float currentShootDelay = 0f;

    private float ShootWaveDelay = 1.5f;
    private float currentShootWaveDelay = 0f;

    private float ShootWaveDuration = 2.5f;
    private float currentShootWaveDuration = 0f;

    bool shooting = false;

    Transform target;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        target = Player.Instance.gameObject.transform;
    }


    new void Update()
    {
        base.Update();
        
        if (shooting)
        {
            //Aims and Shoots
            this.transform.LookAt(target);

            currentShootWaveDuration += Time.deltaTime;
            currentShootDelay += Time.deltaTime;

            if (currentShootWaveDuration >= ShootWaveDuration)
            {
                //Ends the wave with an absorbable
                ShootAbsorbable();
                currentShootWaveDuration = 0;
                shooting = false;
            }

            else if (currentShootDelay >= ShootDelay)
            {
                currentShootDelay = 0;
                Shoot();
            }

            
        }
        else
        {
            //Starts a new wave of attack
            currentShootWaveDelay += Time.deltaTime;
            if (currentShootWaveDelay >= ShootWaveDelay)
            {
                currentShootWaveDelay = 0;
                shooting = true;
            }
        }
    }

    void Shoot()
    {
        objectPooler.SpawnFromPool("SmallBullet", transform.position, transform.rotation);
    }

    void ShootAbsorbable()
    {
        objectPooler.SpawnFromPool("SmallBulletA", transform.position, transform.rotation);
    }
}
