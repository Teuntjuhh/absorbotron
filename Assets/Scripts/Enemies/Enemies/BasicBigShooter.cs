﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicBigShooter : Enemy
{
    private float shootDelay = 3f;
    private float currentShootDelay = 0f;

    private float absorbableChance = 0.25f;

    private float screenShakeDuration = 0.1f;
    private float screenShakeIntensity = 0.2f;

    // Update is called once per frame
    new void Update()
    {
        base.Update();

        currentShootDelay += Time.deltaTime;
        if(currentShootDelay >= shootDelay)
        {
            currentShootDelay = 0;
            Shoot();
        }
    }

    void Shoot()
    {
        if(Random.Range(0f, 1f) < absorbableChance)
        {
            objectPooler.SpawnFromPool("BigBulletA", transform.position - new Vector3(0, 0, 2), transform.rotation);
        }
        else
        {
            objectPooler.SpawnFromPool("BigBullet", transform.position - new Vector3(0, 0, 2), transform.rotation);
        }
        StartCoroutine(Camera.main.GetComponent<Shakeable>().Shake(screenShakeDuration, screenShakeIntensity));
    }
}
