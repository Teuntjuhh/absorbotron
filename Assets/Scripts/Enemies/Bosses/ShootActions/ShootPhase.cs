﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootPhase : MonoBehaviour
{

    public List<ShootAction> ShootActions;
    public int currentAction = 0;

    public void UpdatePhase()
    {
        //loops through shoot actions
        if(ShootActions[currentAction].Shoot())
        {
            transform.localRotation = Quaternion.identity;
            currentAction = (currentAction + 1) % ShootActions.Count;
        }
    }
}
