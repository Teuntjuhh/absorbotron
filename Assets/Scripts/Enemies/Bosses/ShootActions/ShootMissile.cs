﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootMissile : ShootAction
{
    private int currentAmount = 0;
    private float shootDelay = 2.5f;
    private float currentDelay = 0;


    public override bool Shoot()
    {
        currentDelay += Time.deltaTime;

        if (currentDelay >= shootDelay)
        {
            objectPooler.SpawnFromPool("Missile", transform.position, transform.rotation);

            currentDelay = 0;
            currentAmount++;
            if (currentAmount >= Amount)
            {
                currentAmount = 0;
                return true;
            }
        }
        return false;

    }
}
