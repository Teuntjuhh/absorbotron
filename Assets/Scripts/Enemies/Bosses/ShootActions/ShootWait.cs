﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootWait : ShootAction
{
    public float WaitTime;

    private float currentDelay = 0;

    public override bool Shoot()
    {
        currentDelay += Time.deltaTime;

        if (currentDelay >= WaitTime)
        {
            currentDelay = 0;
            return true;
        }
        return false;
    }
}
