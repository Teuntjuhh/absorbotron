﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallSpiral : ShootAction
{
    float shootRotate = -10f;

    private float shootDelay = 0.05f;
    private float currentDelay = 0;

    private int currentAmount = 0;

    new public void Start()
    {
        base.Start();
    }

    public override bool Shoot()
    {
        currentDelay += Time.deltaTime;

        if (currentDelay >= shootDelay)
        {
            objectPooler.SpawnFromPool("SmallBullet", transform.position, transform.rotation);

            transform.Rotate(0, shootRotate, 0);

            currentDelay = 0;
            currentAmount++;
            if (currentAmount >= Amount)
            {
                currentAmount = 0;
                return true;
            }
        }
        return false;
    }
}
