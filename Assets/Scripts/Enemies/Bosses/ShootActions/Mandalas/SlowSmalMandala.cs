﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowSmalMandala : ShootAction
{
    private float absorbableChance = 0.1f;

    float shootRotate = 55f;
    
    private float shootDelay = 0.075f;
    private float currentDelay = 0;

    private int currentAmount = 0;

    new public void Start()
    {
        base.Start();
    }

    public override bool Shoot()
    {
        currentDelay += Time.deltaTime;

        if (currentDelay >= shootDelay)
        {        
            if (Random.Range(0f, 1f) < absorbableChance)
            {
                objectPooler.SpawnFromPool("SlowSmallBulletA", transform.position, transform.rotation);
            }
            else
            {
                objectPooler.SpawnFromPool("SlowSmallBullet", transform.position, transform.rotation);
            }

            transform.Rotate(0, shootRotate, 0);

            currentDelay = 0;
            currentAmount++;
            if (currentAmount >= Amount)
            {
                currentAmount = 0;
                return true;
            }
        }
        return false;
    }
}
