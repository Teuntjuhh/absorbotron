﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimedBigMachineGun : ShootAction
{
    private float screenShakeDuration = 0.1f;
    private float screenShakeIntensity = 0.15f;

    float absorbableChance = 0.3f;

    private int currentAmount = 0;
    private float shootDelay = 1f;
    private float currentDelay = 0;

    public float RotationSpeed;
    Transform target;

    new void Start()
    {
        base.Start();
        target = Player.Instance.gameObject.transform;
    }

    public override bool Shoot()
    {
        currentDelay += Time.deltaTime;

        if (currentDelay >= shootDelay)
        {
            if (Random.Range(0f, 1f) < absorbableChance)
            {
                objectPooler.SpawnFromPool("FastBigBulletA", transform.position, transform.rotation);
            }
            else
            {
                objectPooler.SpawnFromPool("FastBigBullet", transform.position, transform.rotation);
            }

            StartCoroutine(Camera.main.GetComponent<Shakeable>().Shake(screenShakeDuration, screenShakeIntensity));

            currentDelay = 0;
            currentAmount++;
            if (currentAmount >= Amount)
            {
                currentAmount = 0;
                return true;
            }
        }
        else
        {
            Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, RotationSpeed * Time.deltaTime);
        }

        return false;

    }
}
