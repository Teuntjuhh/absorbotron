﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimedHugeShoot : ShootAction
{
    private float screenShakeDuration = 0.2f;
    private float screenShakeIntensity = 0.3f;

    private int currentAmount = 0;
    private float shootDelay = 6f;
    private float currentDelay = 0;

    public float RotationSpeed;
    Transform target;

    new void Start()
    {
        base.Start();
        target = Player.Instance.gameObject.transform;
    }

    public override bool Shoot()
    {
        currentDelay += Time.deltaTime;

        if (currentDelay >= shootDelay)
        {
            objectPooler.SpawnFromPool("HugeBullet", transform.position, transform.rotation);
            StartCoroutine(Camera.main.GetComponent<Shakeable>().Shake(screenShakeDuration, screenShakeIntensity));

            currentDelay = 0;
            currentAmount++;
            if (currentAmount >= Amount)
            {
                currentAmount = 0;
                return true;
            }
        }
        else
        {
            Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, RotationSpeed * Time.deltaTime);
        }

        return false;

    }
}
