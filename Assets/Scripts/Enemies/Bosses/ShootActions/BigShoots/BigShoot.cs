﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigShoot : ShootAction
{
    private float screenShakeDuration = 0.1f;
    private float screenShakeIntensity = 0.2f;

    private int currentAmount = 0;
    private float shootDelay = 2.5f;
    private float currentDelay = 0;


    public override bool Shoot()
    {
        currentDelay += Time.deltaTime;

        if(currentDelay >= shootDelay)
        {
            objectPooler.SpawnFromPool("BigBullet", transform.position, transform.rotation);
            StartCoroutine(Camera.main.GetComponent<Shakeable>().Shake(screenShakeDuration, screenShakeIntensity));

            currentDelay = 0;
            currentAmount++;
            if(currentAmount >= Amount)
            {
                currentAmount = 0;
                return true;
            }
        }
        return false;

    }
}
