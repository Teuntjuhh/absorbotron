﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShootAction : MonoBehaviour
{
    public int Amount;

    public ObjectPooler objectPooler;

    public void Start()
    {
        objectPooler = ObjectPooler.Instance;
    }
    public abstract bool Shoot();
}
