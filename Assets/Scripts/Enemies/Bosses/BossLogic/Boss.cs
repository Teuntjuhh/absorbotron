﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public LevelLoader levelLoader;

    public HealthBar healthBar;

    public GameObject VictoryText;

    public int HP;
    public List<int> PhaseThresholds;

    public MovementAction IntroMovement;
    private int IntroSpeed = 3;

    private float introScreenShakeDuration = 0.1f;
    private float introScreenShakeIntensity = 3f;

    private float explodeDuration = 3f;
    private float explodeIntensity = 0.8f;

    bool Invulnerable = true;
    bool FightStarted = false;
    private bool fightEnded = false;

    public List<BossCannon> BossCannons;

    public List<MovementPhase> MovementPhases;

    public List<Explodeable> explodeables;

    public int currentPhase = 0;

    void Start()
    {
        healthBar.Activate(HP);
    }

    void Update()
    {
        if(!fightEnded)
        {
            if (FightStarted)
            {
                MovementPhases[currentPhase].UpdateMovement();
            }
            else
            {
                if (IntroMovement.Perform(IntroSpeed))
                {
                    FightStart();
                }
                StartCoroutine(Camera.main.GetComponent<Shakeable>().Shake(introScreenShakeDuration, introScreenShakeIntensity));
            }
        }
    }

    void FightStart()
    {
        FightStarted = true;
        Invulnerable = false;

        foreach (BossCannon BC in BossCannons)
        {
            BC.Active = true;
        }
    }

    void NextPhase()
    {
        currentPhase++;
        foreach (BossCannon BC in BossCannons)
        {
            BC.gameObject.transform.localRotation = Quaternion.identity;
            BC.CurrentPhase++;
        }
    }

    public void TakeDamage(int damage)
    {
        if (!Invulnerable)
        {
            HP -= damage;

            healthBar.UpdateHealthBar(HP);

            if(currentPhase < PhaseThresholds.Count)
            {
                if (HP < PhaseThresholds[currentPhase])
                {
                    NextPhase();
                }
            }

            if (HP <= 0)
            {
                StartCoroutine(Die());
            }
        }
    }

    IEnumerator Die()
    {
        fightEnded = true;

        healthBar.gameObject.SetActive(false);

        foreach (Explodeable ex in explodeables)
        {
            StartCoroutine(ex.Explode(explodeDuration, explodeIntensity));
        }

        foreach (BossCannon BC in BossCannons)
        {
            BC.Active = false;
        }

        StartCoroutine(Camera.main.GetComponent<Shakeable>().Shake(introScreenShakeDuration, introScreenShakeIntensity));

        float currentExplodeDuration = 0;


        while (currentExplodeDuration < explodeDuration)
        {
            currentExplodeDuration += Time.deltaTime;
            yield return null;
        }

        levelLoader.GameOver = true;
        VictoryText.SetActive(true);

        gameObject.SetActive(false);
    }

}
