﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCannon : MonoBehaviour
{
    public List<ShootPhase> ShootPhases;

    public int CurrentPhase = 0;

    public bool Active = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Active)
        {
            ShootPhases[CurrentPhase].UpdatePhase();
        }
    }
}
