﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPhase : MonoBehaviour
{
    public List<MovementAction> MovementActions;
    public float Speed;

    private int currentAction = 0;

    public void UpdateMovement()
    {
        if(MovementActions[currentAction].Perform(Speed))
        {
            currentAction++;
            currentAction = currentAction % MovementActions.Count;
        }
    }
}
