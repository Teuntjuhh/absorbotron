﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniBoss : Enemy
{
    public List<ShootAction> ShootActions;

    private int currentShootAction = 0;

    new void Update()
    {
        base.Update();

        if (currentShootAction < movementActions.Count)
        {
            if (ShootActions[currentShootAction].Shoot())
            {
                currentShootAction++;
                currentShootAction = currentShootAction % ShootActions.Count;
            }
        }
    }
}
