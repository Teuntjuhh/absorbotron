﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCannon : Enemy
{
    public HealthBar healthBar;

    private float RotationSpeed = 15;

    private float screenShakeDuration = 0.3f;
    private float screenShakeIntensity = 0.5f;

    private float shootDelay = 5f;
    private float currentShootDelay = 0f;

    Transform target;

    new void Start()
    {
        base.Start();
        target = Player.Instance.gameObject.transform;
        healthBar.Activate(HP);
    }

    new void Update()
    {
        base.Update();

        currentShootDelay += Time.deltaTime;
        if (currentMovementAction > 0)
        {
            Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, RotationSpeed * Time.deltaTime);

            if (currentShootDelay >= shootDelay)
            {
                currentShootDelay = 0;
                Shoot();
            }
        }

        healthBar.UpdateHealthBar(HP);
    }

    void Shoot()
    {
        objectPooler.SpawnFromPool("HugeBullet", transform.position, transform.rotation);
        StartCoroutine(Camera.main.GetComponent<Shakeable>().Shake(screenShakeDuration, screenShakeIntensity));

        currentShootDelay = 0;
    }

    public override IEnumerator Die()
    {
        healthBar.gameObject.SetActive(false);
        return base.Die();
    }
}
