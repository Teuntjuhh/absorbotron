﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    public int Damage;
    public float Speed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Update()
    {
        Move();
    }

    public int HitPlayer()
    {       
        gameObject.SetActive(false);
        return Damage;
    }

    public virtual void Move()
    {
        transform.Translate(new Vector3(0, 0, Speed * Time.deltaTime), Space.Self);
    }

    public virtual void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponentInParent<Player>();
        if(player != null)
        {
            player.TakeDamage(HitPlayer());
        }
    }
}
