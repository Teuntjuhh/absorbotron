﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseMissile : EnemyBullet
{

    public Rigidbody RB;
    public float MaxSpeed;
    public float Acceleration;

    public float RotationSpeed;

    Transform target;

    // Start is called before the first frame update
    void Start()
    {
        target = Player.Instance.gameObject.transform;
    }

    public override void Move()
    {
        Quaternion targetRotation = Quaternion.LookRotation(target.position - transform.position);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, RotationSpeed * Time.deltaTime);

        RB.velocity += RB.transform.forward * Acceleration * Time.deltaTime;
        RB.velocity = Vector3.ClampMagnitude(RB.velocity, MaxSpeed);
    }


    public override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        PlayerBulletBasic playerBullet = other.gameObject.GetComponent<PlayerBulletBasic>();

        if(playerBullet != null)
        {
            playerBullet.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }
    }
}
