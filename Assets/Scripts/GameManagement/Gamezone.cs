﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamezone : MonoBehaviour
{
    private void OnTriggerExit(Collider other)
    {
        if(other.transform.parent !=null)
        {
            other.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            other.gameObject.SetActive(false);
        }
    }
}
