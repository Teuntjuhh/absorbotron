﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    public List<EnemySpawnGroup> groups;

    public bool StageOver = false;

    int currentActiveGroup = 0;

    // Start is called before the first frame update
    void Start()
    {
        groups[0].gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if(!StageOver)
        {
            if (groups[currentActiveGroup].WaveOver)
            {
                currentActiveGroup++;

                if (currentActiveGroup < groups.Count)
                {
                    groups[currentActiveGroup].gameObject.SetActive(true);

                }
                else
                {
                    EndStage();
                }
            }
        }     
    }

    void EndStage()
    {
        StageOver = true;
    }
}
