﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnGroup : MonoBehaviour
{
    public float Duration;
    public bool WaveOver;

    [SerializeField]
    public List<GameObject> SpawnedObjects;

    ObjectPooler objectPooler;

    private float elapsedTime;

   
    // Start is called before the first frame update
    void Start()
    {
 
    }

    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;

        if (elapsedTime > Duration || !CheckIfActiveObjects())
        {
            WaveOver = true;
        }
        
    }

    bool CheckIfActiveObjects()
    {
        foreach (GameObject obj in SpawnedObjects)
        {
            if(obj.activeInHierarchy)
            {
                return true;
            }
        }

        return false;
    }

    private void OnEnable()
    {
        
    }
}