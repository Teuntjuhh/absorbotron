﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private int startingHealth;

    public Image image;

    public void Activate(int HP)
    {
        startingHealth = HP;
        gameObject.SetActive(true);
    }

    public void UpdateHealthBar(int HP)
    {
        image.fillAmount = (HP * 1.0f) / (startingHealth * 1.0f);
    }
}
