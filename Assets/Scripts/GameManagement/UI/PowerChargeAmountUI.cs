﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerChargeAmountUI : MonoBehaviour
{
    public RectTransform rectTransform;

    private int imageHeight = 50;
    private int imageWidth = 50;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateCharge(int max, int current)
    {
        float fraction = (float)current / (float)max;
        rectTransform.localPosition = new Vector3(0, fraction * imageHeight / 2, 0);
        rectTransform.sizeDelta = new Vector2(imageWidth, imageHeight - fraction * imageHeight);
    }
}
