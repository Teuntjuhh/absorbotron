﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Player player;

    public List<PowerUI> PowerPositions;
    public List<PowerChargeAmountUI> ChargeAmountUIs;

    public Image None;
    public Image Big;
    public Image Quick;


    void Update()
    {
        UpdatePowersUI();
    }

    //assigns power UI according to the player's powerlist
    void UpdatePowersUI()
    {
        for (int i = 0; i < player.MaxPowers; i++)
        {
            if(i < PowerPositions.Count && i < ChargeAmountUIs.Count)
            {
                Image image;
                if(player.StoredPowers.Count > i)
                {
                    image = GetPowerImage(player.StoredPowers[i]);
                    ChargeAmountUIs[i].UpdateCharge(player.StoredPowers[i].MaxCharge, player.StoredPowers[i].CurrentCharge);
                }
                else
                {
                    image = None;
                }

                PowerPositions[i].UpdateImage(image);
                
            }
            else
            {
                Debug.Log("Not enough UI spaces to show all stored powers");
            }
        }
    }

    Image GetPowerImage(Power power)
    {
        switch (power.type)
        {
            case PowerType.Big:
                return Big;
            case PowerType.Quick:
                return Quick;
            case PowerType.Dual:
                break;
            case PowerType.Explosive:
                break;
            default:
                break;
        }


        Debug.Log("Image not found");
        return null;
    }
}
