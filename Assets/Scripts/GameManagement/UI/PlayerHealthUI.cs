﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : MonoBehaviour
{
    public Text HPtext;

    float damageFlashTime = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        HPtext.color = Color.green;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetHP(int hp)
    {
        HPtext.text = "HP: " + hp.ToString();
    }

    IEnumerator DamageFlash()
    {
        float currentDelay= 0f;

        HPtext.color = Color.red;

        while(currentDelay < damageFlashTime)
        {
            currentDelay += Time.deltaTime;
            yield return null;
        }

        HPtext.color = Color.green;
    }
}
