﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Power
{
    public PowerType type;
    public int MaxCharge;
    public int CurrentCharge;

    public void Start()
    {
        
    }

    public bool Use()
    {
        CurrentCharge -= 1;

        if(CurrentCharge <= 0)
        {
            return true;
        }

        return false;
    }
}
