﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Absorbable : MonoBehaviour
{
    public Power AbsorbablePower;

    public float Speed;

    private void Update()
    {
        transform.Translate(new Vector3(0, 0, Speed * Time.deltaTime), Space.Self);
        
    }

    public Power Absorb()
    {
        gameObject.SetActive(false);
        return AbsorbablePower;
    }
}
