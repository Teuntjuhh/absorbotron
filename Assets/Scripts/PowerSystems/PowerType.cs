﻿using UnityEngine;
using System.Collections;

public enum PowerType
{
    Big,
    Quick,
    Dual,
    Explosive
}
