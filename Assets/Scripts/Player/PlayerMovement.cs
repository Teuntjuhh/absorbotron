﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    public GameObject Model;
    private float MaxSpeed = 20;
    private float Acceleration = 200;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movement = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        movement = movement.normalized * Time.deltaTime * Acceleration;

        rb.velocity += movement;
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, MaxSpeed);

        Model.transform.eulerAngles = new Vector3(0, 0, rb.velocity.x);
    }
}
