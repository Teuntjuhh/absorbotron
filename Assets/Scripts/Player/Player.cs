﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public LevelLoader levelLoader;

    public Weapon weapon;
    public Shakeable shakeable;
    public List<Power> StoredPowers;

    public PlayerHealthUI healthUI;
    public GameObject GameOverUI;

    private Renderer[] renderers;
    private Color normalEmission;

    public int HP;
    public int MaxPowers = 3;

    private float damageFlashIntensity = 7f;
    private float damageFlashTime = 0.1f;
    private float damageShakeDurationModifier = 0.005f;
    private float damageShakeIntensityModifier = 0.03f;

    private float currentShootDelay = 0;
    private bool invulnerable = false;

    public static Player Instance;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        renderers = GetComponentsInChildren<Renderer>();

        normalEmission = renderers[0].material.GetColor("_EmissionColor");
        StoredPowers = new List<Power>();
    }

    void Update()
    {
        UpdateShoot();
    }


    //adds new power to the powerlist if there is space
    public void Absorb(Power power)
    {
        if(StoredPowers.Count < MaxPowers)
        {
            Power newPower = new Power();
            newPower.type = power.type;
            newPower.CurrentCharge = power.CurrentCharge;
            newPower.MaxCharge = power.MaxCharge;

            StoredPowers.Add(newPower);
        }
    }

    //shoots the playerweapon
    void UpdateShoot()
    {
        currentShootDelay -= Time.deltaTime;
        if (currentShootDelay <= 0 && Input.GetButton("Fire1"))
        {
            currentShootDelay = weapon.Shoot(StoredPowers);

            List<Power> toRemove = new List<Power>();

            foreach(Power p in StoredPowers)
            {
                if(p.Use())
                {
                    toRemove.Add(p);
                }
            }

            foreach(Power p in toRemove)
            {
                StoredPowers.Remove(p);
            }
        }
    }

    void UpdateHealthUI()
    {
        healthUI.SetHP(HP);
    }

    public void TakeDamage(int damage)
    {
        if(!invulnerable)
        {
            invulnerable = true;
            HP -= damage;
            UpdateHealthUI();
            StartCoroutine("DamageFlash");
            healthUI.StartCoroutine("DamageFlash");

            if (shakeable != null)
            {
                StartCoroutine(shakeable.Shake(damageShakeDurationModifier * damage, damageShakeIntensityModifier * damage));
            }

            if (HP <= 0)
            {
                GameOver();
            }
        }
        
    }


    //shortly flashes all the models in the object
    IEnumerator DamageFlash()
    {
        float currentDelay = 0f;

        foreach (Renderer ren in renderers)
        {
            ren.material.SetColor("_EmissionColor", Color.yellow * damageFlashIntensity);
        }

        while (currentDelay < damageFlashTime)
        {
            currentDelay += Time.deltaTime;
            yield return null;
        }

        foreach (Renderer ren in renderers)
        {
            ren.material.SetColor("_EmissionColor", normalEmission);
        }

        invulnerable = false;
    }

    void GameOver()
    {
        gameObject.SetActive(false);
        GameOverUI.SetActive(true);
        levelLoader.GameOver = true;
    }

    private void OnTriggerEnter(Collider collider)
    {
        Absorbable currentAbsorb = collider.gameObject.GetComponent<Absorbable>();

        if (currentAbsorb != null)
        {
            Absorb(currentAbsorb.Absorb()); 
        }
    }
}
