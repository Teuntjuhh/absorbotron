﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Weapon : MonoBehaviour
{
    ObjectPooler objectPooler;

    public Rigidbody rb;

    float recoilModifier = 10f;

    float BasicDelay = 0.5f;
    
    void Start()
    {
        objectPooler = ObjectPooler.Instance;
    }

    //returns the delay before the next bullet is shot
    public float Shoot(List<Power> powers)
    {
        if(powers.Count == 0)
        {
            return BasicShoot();          
        }
        else
        {
            PowerType mainPower = FindMainPower(powers);

            Recoil(powers);

            switch (mainPower)
            {
                case PowerType.Big:
                    return BigShoot(powers);
                case PowerType.Quick:
                    return QuickShoot(powers);
                case PowerType.Dual:
                    break;
                case PowerType.Explosive:
                    break;
                default:
                    break;
            }

        }

        return 1f;
    }

    private float BasicShoot()
    {
        objectPooler.SpawnFromPool("PlayerBulletBasic", transform.position, transform.rotation);
        return BasicDelay;
    }

    private float BigShoot(List<Power> powers)
    {
        GameObject bigBullet = objectPooler.SpawnFromPool("PlayerBulletBig", transform.position, transform.rotation);
        return bigBullet.GetComponent<PlayerBulletBig>().Shoot(powers);
    }

    private float QuickShoot(List<Power> powers)
    {
        GameObject quickBullet = objectPooler.SpawnFromPool("PlayerBulletQuick", transform.position, transform.rotation);
        return quickBullet.GetComponent<PlayerBulletQuick>().Shoot(powers);
    }

    private PowerType FindMainPower(List<Power> powers)
    {
        List<PowerType> powerTypes = new List<PowerType>();

        foreach(Power p in powers)
        {
            powerTypes.Add(p.type);
        }

        var groupsWithCounts = from pt in powerTypes
                               group pt by pt into g
                               select new
                               {
                                   Item = g.Key,
                                   Count = g.Count()
                               };

        var groupsSorted = groupsWithCounts.OrderByDescending(g => g.Count);
        return groupsSorted.First().Item;
    }

    private void Recoil(List<Power> powers)
    {
        float recoil = 0;
        foreach(Power p in powers)
        {
            if(p.type == PowerType.Big)
            {
                recoil += recoilModifier;
            }
        }

        rb.AddForce(recoil * -transform.forward, ForceMode.Impulse);
    }
}