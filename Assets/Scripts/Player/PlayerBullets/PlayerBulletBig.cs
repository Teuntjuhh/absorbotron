﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletBig : PlayerBulletBasic
{
    void Start()
    {
        StartingSpeed = 15;
        StartingDelay = 0.75f;
        bigSizeModifier = 1.2f;
    }


}
