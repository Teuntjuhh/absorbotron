﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletBasic : MonoBehaviour
{
    public int StartingDamage = 5;
    public float StartingSpeed = 25;
    public float StartingDelay = 0.5f;

    public int Damage;
    public float Speed;
    public float Delay;

    public float bigSizeModifier = 0.8f;
    public int bigDamageModifier = 5;

    private float screenShakeDurationModifier = 0.1f;
    private float screenShakeIntensityModifier = 0.2f;
    private float screenShakeDuration = 0;
    private float screenShakeIntensity = 0;

    public float quickDelayModifier = 0.4f;
    public float quickSpeedModifier = 5;
 

    public void Update()
    {
        transform.Translate(new Vector3(0, 0, Speed * Time.deltaTime), Space.Self);
    }

    public float Shoot(List<Power> powers)
    {
        Damage = StartingDamage;
        Speed = StartingSpeed;
        Delay = StartingDelay;

        foreach (Power p in powers)
        {
            switch (p.type)
            {
                case PowerType.Big:
                    AddBig();
                    break;
                case PowerType.Quick:
                    AddQuick();
                    break;
                case PowerType.Dual:
                    break;
                case PowerType.Explosive:
                    break;
                default:
                    break;
            }
        }

        if(screenShakeIntensity > 0)
        {
            ScreenShake();
        }
        
        return Delay;
    }

    private void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.gameObject.GetComponentInParent<Enemy>();

        if (enemy != null)
        {
            enemy.TakeDamage(Damage);
            gameObject.SetActive(false);
            return;
        }

        Boss boss = other.gameObject.GetComponentInParent<Boss>();

        if (boss != null)
        {
            boss.TakeDamage(Damage);
            gameObject.SetActive(false);
            return;
        }
    }

    private void AddBig()
    {
        Damage += bigDamageModifier;
        gameObject.transform.localScale += new Vector3(bigSizeModifier, bigSizeModifier, bigSizeModifier);
        screenShakeDuration += screenShakeDurationModifier;
        screenShakeIntensity += screenShakeIntensityModifier;
    }

    private void AddQuick()
    {
        Delay -= Delay * quickDelayModifier;
        Speed += quickSpeedModifier;
    }

    private void ScreenShake()
    {
        StartCoroutine(Camera.main.GetComponent<Shakeable>().Shake(screenShakeDuration, screenShakeIntensity));
    }
}
