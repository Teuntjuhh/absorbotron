﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourLoop : MonoBehaviour
{
    public List<Color> ColorLoop;
    public float EmissionIntensity;
    public float colourChangeDuration;

    new private Renderer renderer;

    private float currentColourLerpDuration = 0f;
    private int currentColorIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        LoopColour();
    }

    //loops through the colour list by lerping
    void LoopColour()
    {
        renderer.material.SetColor("_EmissionColor", Color.Lerp(ColorLoop[currentColorIndex] * EmissionIntensity, ColorLoop[(currentColorIndex + 1) % ColorLoop.Count], currentColourLerpDuration));

        currentColourLerpDuration += Time.deltaTime / colourChangeDuration;

        if (currentColourLerpDuration >= 1)
        {
            currentColourLerpDuration = 0;
            currentColorIndex = (currentColorIndex + 1) % ColorLoop.Count;
        }
    }
}
