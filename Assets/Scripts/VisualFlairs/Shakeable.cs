﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shakeable : MonoBehaviour
{
    Vector3 originalPos;

    private bool shaking = false;

    void OnEnable()
    {
        originalPos = transform.localPosition;
    }

    public IEnumerator Shake(float shakeDuration, float shakeAmount)
    {
        if(shaking)
        {
            yield return null;
        }

        shaking = true;

        float currentShakeDuration = 0;

        while (currentShakeDuration < shakeDuration)
        {
            float reductionFactor = currentShakeDuration / shakeDuration * shakeAmount;

            transform.localPosition = originalPos + Random.insideUnitSphere * (shakeAmount - reductionFactor);

            currentShakeDuration += Time.deltaTime;
            yield return null;
        }

        shaking = false;

        transform.localPosition = originalPos;
    }

}
