﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explodeable : MonoBehaviour
{
    private float colourFlashDuration = 0.15f;
    private float emissionIntensity = 50;

    new Renderer renderer;

    private void Start()
    {
        renderer = GetComponent<Renderer>();
    }

    public IEnumerator Explode(float explodeDuration, float shakeAmount)
    {
        Vector3 originalPos = transform.localPosition;
        float currentExplodeDuration = 0;

        while(currentExplodeDuration < explodeDuration)
        {
            currentExplodeDuration += Time.deltaTime;

            transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;


            //alternates between red and yellow
            if (currentExplodeDuration % (colourFlashDuration * 2) > colourFlashDuration)
            {
                renderer.material.SetColor("_EmissionColor", Color.red * emissionIntensity);
            }
            else
            {
                renderer.material.SetColor("_EmissionColor", Color.yellow * emissionIntensity);
            }
            yield return null;
        }
        
    }
}